package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if(inputNumbers.contains(null)) throw new CannotBuildPyramidException();

        int rows = getRow(inputNumbers);

        if(rows == -1) throw new CannotBuildPyramidException();
        int cols = 2 * rows - 1;
        Collections.sort(inputNumbers);
        int[][] pyramid = new int[rows][cols];

        boolean flag;
        int ind = 0;
        for (int i = 0; i < rows; i++) {
            flag = false;
            for (int j = 0; j < rows - 1 - i; j++)
                pyramid[i][j] = 0;
            for (int j = rows - 1 - i; j < rows + i; j++) {
                if (flag) {
                    pyramid[i][j] = 0;
                }
                else {
                    pyramid[i][j] = inputNumbers.get(ind++);
                }
                flag = !flag;
            }
            for (int j = rows + i; j < cols; j++)
                pyramid[i][j] = 0;
        }
        return pyramid;
    }

    private static int getRow(List<Integer> input)
    {
        double res = (Math.sqrt(1+ 8 * input.size()) - 1)/2;
        if(res == Math.ceil(res)){
            return (int)res;
        }
        return -1;

    }
}
